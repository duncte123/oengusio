package app.oengus.entity.model;

public enum RunType {

	SINGLE,
	RACE,
	COOP,
	COOP_RACE,
	OTHER

}
