export const environment = {
  production: true,
  sandbox: false,
  baseSite: 'https://oengus.io',
  api: 'https://oengus.io/api',
  twitchLoginClientId: 'rdbunr40rghklvyvv78eblcptbg6hh',
  twitchSyncClientId: 'f3cxbed85hy87sqa6nlv96uvhvqws7',
  loginRedirect: 'https://oengus.io/login/',
  syncRedirect: 'https://oengus.io/user/settings/sync/',
  matomoId: 1,
  paypalClientId: 'AZdzNo_y3Y_JyXJp_Dbv5ulY5EHinZo8S0OFNUyoCe7U9vj-1MfR9xbf0VIja6KYBUAey0tNOrubxT2b',
  closingDate: new Date(2021, 6, 1, 0, 0, 0)
};
